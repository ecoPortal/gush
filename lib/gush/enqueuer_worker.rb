require 'active_job'

module Gush
  class EnqueuerWorker < ::ActiveJob::Base

    def perform(workflow_id)
      workflow = client.find_workflow(workflow_id)
      return if workflow.stopped

      jobs_to_be_checked = client.jobs_to_be_enqueued(workflow_id)
      jobs_to_be_enqueued = jobs_to_be_checked.select { |job| job.ready_to_start? }

      if jobs_to_be_enqueued.any?
        jobs_that_will_be_enqueued = jobs_to_be_enqueued.take(10000) # avoid putting too much pressure into the queue
        client.enqueue_jobs(workflow_id, jobs_that_will_be_enqueued, true)
        client.remove_from_delayed_enqueue_queue(workflow_id, jobs_that_will_be_enqueued)  # we don't really need to remove those jobs from the queue since we check if they can be started, but is always better to work with a smaller queue
      end
      client.enqueue_enqueuer_worker(workflow_id, workflow.enqueuer_frequency) unless workflow.finished? #not sure if I should put this inside an ensure block or let the active job retry system handle it
    end

    private

    attr_reader :client, :workflow_id, :job

    def client
      @client ||= Gush::Client.new(Gush.configuration)
    end
  end
end
